package services.service.addService.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
 
import static org.hamcrest.Matchers.*;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TestAddController {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void serciceDescription() throws Exception {
        mockMvc.perform(get("/service-description"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("status", is(HttpStatus.OK.name())))
                    .andExpect(jsonPath("result", is("Returns leftOperand added to rightOperand")));
    }

    @Test
    public void serciceOperands() throws Exception {
        mockMvc.perform(get("/service-operands"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("status", is(HttpStatus.OK.name())))
                    .andExpect(jsonPath("result", is("leftOperand,rightOperand")));
    }

    @Test
    public void operateAddsSuccessfully() throws Exception {
        mockMvc.perform(get("/operate?leftOperand=32&rightOperand=34"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("status", is(HttpStatus.OK.name())))
                .andExpect(jsonPath("result", is("66")));
    }

    @Test
    public void operateNoLeftOperand() throws Exception {
        mockMvc.perform(get("/operate?rightOperand=34"))
                   .andExpect(status().isOk())
                   .andExpect(jsonPath("status", is(HttpStatus.INTERNAL_SERVER_ERROR.name())))
                   .andExpect(jsonPath("error", is("invalid parameters: rightOperand")));
    }

    @Test
    public void operateNoRightOperand() throws Exception {
        mockMvc.perform(get("/operate?leftOperand=34"))
                   .andExpect(status().isOk())
                   .andExpect(jsonPath("status", is(HttpStatus.INTERNAL_SERVER_ERROR.name())))
                   .andExpect(jsonPath("error", is("invalid parameters: leftOperand")));
    }

    @Test
    public void operateIncorrectParam() throws Exception {
        mockMvc.perform(get("/operate?leftOperand=aa&rightOpera=34"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("status", is(HttpStatus.INTERNAL_SERVER_ERROR.name())))
                    .andExpect(jsonPath("error", is("invalid parameters: leftOperand,rightOpera")));
    }

    @Test
    public void operateAddsNegative() throws Exception {
        mockMvc.perform(get("/operate?leftOperand=-32&rightOperand=34"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("status", is(HttpStatus.OK.name())))
                    .andExpect(jsonPath("result", is("2")));
    }
    
    @Test
    public void operateAddsNonNumeric() throws Exception {
        mockMvc.perform(get("/operate?leftOperand=aa&rightOperand=34"))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("status", is(HttpStatus.BAD_REQUEST.name())))
                    .andExpect(jsonPath("error", is("Problem decoding params: For input string: \"aa\"")));
    }
}
