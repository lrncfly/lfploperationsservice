package services.service.addService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Basic Add service spring boot application. It will add two operands
 * @author laurencefoley
 *
 */
@EnableDiscoveryClient
@SpringBootApplication
public class AddService {

    public static void main(String[] args) {
        SpringApplication.run(AddService.class, args);
    }
}