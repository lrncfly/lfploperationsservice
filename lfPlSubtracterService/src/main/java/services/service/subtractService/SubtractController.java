package services.service.subtractService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import services.service.operationsService.Result;
import services.service.operationsService.exceptions.OperationException;
import services.service.operationsService.Operatable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Subtract service returns the difference between the two operands supplied
 *
 * @author laurencefoley
 */
@RestController
public class SubtractController implements Operatable {
    private Logger logger = LoggerFactory.getLogger(SubtractController.class);

    @Override
    public Result getDescription() {
        return new Result(HttpStatus.OK, "Returns rightOperand subtracted from leftOperand", null);
    }

    @Override
    public Result getOperands() {
        List<String> operands = new ArrayList<>();
        operands.add("leftOperand");
        operands.add("rightOperand");
        return new Result(HttpStatus.OK, String.join(",", operands), null);
    }

    @Override
    public Result operate(@RequestParam Map<String, String> params) {
        String paramKeys = String.join(",", params.keySet());
        String paramValues = String.join(",", params.values());
        logger.info(String.format("Subtract Service received request to subtract %s", String.join(",", paramValues)));
        Result result;
        try {
            String left = params.get("leftOperand");
            String right = params.get("rightOperand");
            if (left != null && right != null) {
                Integer leftOperand = Integer.decode(left);
                Integer rightOperand = Integer.decode(right);
                String output = this.doOperate(leftOperand, rightOperand);
                result = new Result(HttpStatus.OK, output, null);
            } else {
                result = new Result(HttpStatus.INTERNAL_SERVER_ERROR, null, String.format("invalid parameters: %s", paramKeys));
            }
        } catch (NumberFormatException nfe) {
            logger.error(String.format("Problem decoding params: %s", nfe.getMessage()));
            result = new Result(HttpStatus.BAD_REQUEST, null, String.format("Problem decoding params: %s", nfe.getMessage()));
        }
        return result;
    }

    private String doOperate(Integer leftOperand, Integer rightOperand) {
        logger.info(String.format("Subtract Service received request to subtract %s from %s", rightOperand, leftOperand));
        String result;
        try {
            result = Integer.toString(leftOperand - rightOperand);
        } catch (Exception e) {
            String error = String.format("Subtract Service encountered an error when trying to subtract %s from %s : %s", rightOperand, leftOperand, e.getMessage());
            logger.error(error);
            throw new OperationException(error);
        }
        return result;
    }
}
