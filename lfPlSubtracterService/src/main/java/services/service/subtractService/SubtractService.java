package services.service.subtractService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Basic Subtract service spring boot application. It will subtract two operands
 * @author laurencefoley
 *
 */
@EnableDiscoveryClient
@SpringBootApplication
public class SubtractService {

    public static void main(String[] args) {
        SpringApplication.run(SubtractService.class, args);
    }
}