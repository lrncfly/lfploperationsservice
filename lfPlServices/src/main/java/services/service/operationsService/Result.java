package services.service.operationsService;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpStatus;

/**
 * Class to encapsulate the responses from each of the service operations
 *
 * @author laurencefoley
 */
public class Result {
    private final String error;
    private final String result;
    private final HttpStatus status;

    /**
     * @param status The status of this result operation
     * @param result The value of this result operation. Null if error.
     * @param error  The error description if an error occurs.
     */
    @JsonCreator
    public Result(@JsonProperty("status") HttpStatus status, @JsonProperty("result") String result, @JsonProperty("error") String error) {
        this.status = status;
        this.result = result;
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public String getResult() {
        return result;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
