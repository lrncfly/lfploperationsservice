package services.service.operationsService.service;



import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Encapsulates the service instance description and operands of a registered service
 * @author laurencefoley
 *
 */
public class LfPlServiceInstance extends ResourceSupport {

    private final String description;
    private final String operands;

    @JsonCreator
    public LfPlServiceInstance(@JsonProperty("description") String description, @JsonProperty("operands") String operands) {
        this.description = description;
        this.operands = operands;
    }

    public String getDescription() {
        return description;
    }

    public String getOperands() {
        return operands;
    }
}
