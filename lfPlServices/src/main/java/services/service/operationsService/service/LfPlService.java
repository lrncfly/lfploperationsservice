package services.service.operationsService.service;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Encapsulates a service registered with the Eureka service
 * @author laurencefoley
 *
 */
public class LfPlService extends ResourceSupport {

    private final String serviceName;

    @JsonCreator
    public LfPlService(@JsonProperty("name") String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceName() {
        return serviceName;
    }
}
