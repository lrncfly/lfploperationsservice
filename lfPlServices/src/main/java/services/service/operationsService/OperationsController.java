package services.service.operationsService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import services.service.operationsService.exceptions.OperationException;
import services.service.operationsService.exceptions.ServiceNotFoundException;
import services.service.operationsService.service.LfPlService;
import services.service.operationsService.service.LfPlServiceInstance;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 * Controller that manages client interaction to {@link Operatable} service endpoints.
 *
 * @author laurencefoley
 */
@RestController
public class OperationsController {
    Logger logger = LoggerFactory.getLogger(OperationsController.class);

    //To look up the Eureka service
    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private RestTemplate restTemplate;

    //To find suitable service from the discoveryClient 
    @Autowired
    private LoadBalancerClient loadBalancer;

    @Value("${spring.application.name}")
    private String applicationName;

    /**
     * The root endpoint for this service. Queries the Eureka service for registered services and generates links to
     * them.
     *
     * @return a list of {@link LfPlService} registered with the Eureka service
     */
    @RequestMapping(method = RequestMethod.GET, value = "/")
    public List<LfPlService> listServices() {
        List<String> discoveredServices = this.discoveryClient.getServices();
        List<LfPlService> serviceListing = new ArrayList<>();
        for (String serviceName : discoveredServices) {
            if (serviceName.compareTo(applicationName) != 0) {
                LfPlService service = new LfPlService(serviceName);
                service.add(linkTo(OperationsController.class).slash(serviceName).withSelfRel());
                serviceListing.add(service);
            }
        }
        return serviceListing;
    }

    /**
     * Return basic information for the serviceName service.
     *
     * @param serviceName the service to query
     * @return a {@link LfPlServiceInstance} populated with details on this service
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{serviceName}")
    public LfPlServiceInstance operator(@PathVariable String serviceName) {
        logger.info("using discoveryClient to find " + serviceName);
        ServiceInstance instance = loadBalancer.choose(serviceName);
        if (instance != null) {
            logger.info(String.format("Found instance of %s: ", serviceName, instance.getHost()));
            URI descriptionUri = instance.getUri();
            descriptionUri = descriptionUri.resolve(String.format("/service-description"));
            URI operandUri = instance.getUri();
            operandUri = operandUri.resolve(String.format("/service-operands"));
            Result description = restTemplate.getForObject(descriptionUri, Result.class);
            Result operands = restTemplate.getForObject(operandUri, Result.class);
            if (description.getStatus() == HttpStatus.OK && operands.getStatus() == HttpStatus.OK) {
                LfPlServiceInstance serviceInstance = new LfPlServiceInstance(description.getResult(), operands.getResult());
                serviceInstance.add(linkTo(OperationsController.class).slash(serviceName).slash("operate").withSelfRel());
                return serviceInstance;
            }
            String message = String.format("Error querying service %s: %s, %s", serviceName, description.getError(), operands.getError());
            logger.error(message);
            throw new ServiceNotFoundException(message);
        }
        String message = String.format("No instances of %s available", serviceName);
        logger.error(message);
        throw new ServiceNotFoundException(message);
    }

    /**
     * Performs a request to the serviceName service with the operands, and returns the result from that service as a
     * {@link Result}
     *
     * @param serviceName The service name to operate on
     * @param operands    The operands for the operation
     * @return A {@link Result} populated with the result of the operation
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{serviceName}/operate")
    public Result operate(@PathVariable String serviceName, @RequestParam Map<String, String> operands) {
        logger.info("using discoveryClient to find " + serviceName);
        ServiceInstance instance = loadBalancer.choose(serviceName);

        if (instance != null) {
            logger.info(String.format("Found instance of %s: ", serviceName, instance.getHost()));
            String requestParams = generateOperandString(operands);
            URI operationUri = instance.getUri();
            operationUri = operationUri.resolve(String.format("/operate" + requestParams));
            try {
                return restTemplate.getForObject(operationUri, Result.class);
            } catch (Exception e) {
                //TODO message is not getting through to here. Status is though
                logger.error(String.format("Problem in response: %s", e.getMessage()));
                throw new OperationException(e.getMessage());
            }
        }
        String message = String.format("No instances of %s available", serviceName);
        logger.error(message);
        throw new ServiceNotFoundException(message);
    }

    /**
     * Generate a request parameter string from the supplied params.
     *
     * @param params The request parameters
     * @return A string formatted as "?key1=value1&...keyn=valuen"
     */
    private String generateOperandString(Map<String, String> params) {
        if (params.isEmpty()) {
            return "";
        } else {
            StringBuilder sb = new StringBuilder("?");
            for (String key : params.keySet()) {
                sb.append(key).append("=").append(params.get(key)).append("&");
            }
            sb.deleteCharAt(sb.length() - 1);
            return sb.toString();
        }
    }
}