package services.service.operationsService;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * Interface that each service must implement.
 *
 * @author laurencefoley
 */
public interface Operatable {
    /**
     * @return A {@link Result} containing the description of this service
     */
    @RequestMapping("/service-description")
    public Result getDescription();

    /**
     * @return A {@link Result} containing the operands required by {@link #operate}
     */
    @RequestMapping("/service-operands")
    public Result getOperands();

    /**
     * @param operands {@link #getOperands} required by this service as key value pair
     * @return A {@link Result} containing the result of the operation on the operands parameters
     */
    @RequestMapping("/operate")
    public Result operate(@RequestParam Map<String, String> operands);
}
