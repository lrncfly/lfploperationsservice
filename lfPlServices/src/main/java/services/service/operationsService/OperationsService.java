package services.service.operationsService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Client facing service spring boot application.
 *
 * @author laurencefoley
 */
@SpringBootApplication
public class OperationsService {

    public static void main(String[] args) {
        SpringApplication.run(OperationsService.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}