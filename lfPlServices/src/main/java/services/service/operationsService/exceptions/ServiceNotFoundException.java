package services.service.operationsService.exceptions;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception to throw if a request is made to a service that is not found
 * @author laurencefoley
 *
 */
@ResponseStatus(value=HttpStatus.NOT_FOUND)
public class ServiceNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ServiceNotFoundException(String message) {
        super(message);
    }
}