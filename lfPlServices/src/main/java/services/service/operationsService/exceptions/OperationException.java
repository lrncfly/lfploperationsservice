package services.service.operationsService.exceptions;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception thrown if an exception is thrown during service operation
 * @author laurencefoley
 *
 */
@ResponseStatus(value=HttpStatus.UNPROCESSABLE_ENTITY) 
public class OperationException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public OperationException(String message) {
        super(message);
    }
}