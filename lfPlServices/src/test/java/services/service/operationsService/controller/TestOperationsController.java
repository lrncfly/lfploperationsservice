package services.service.operationsService.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.*;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.mock.http.client.MockClientHttpResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.net.URI;
import java.util.Collections;

import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(properties = {
        "spring.application.name=service0",
        "spring.cloud.discovery.client.simple.instances.service1[0].uri=http://s1-1:12345",
        "spring.cloud.discovery.client.simple.instances.service2[1].uri=https://s2-1:54321"})
public class TestOperationsController {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private LoadBalancerClient client;
    private LoadBalancerRetryProperties lbProperties;
    private LoadBalancerRequestFactory lbRequestFactory;
    // private LoadBalancedBackOffPolicyFactory backOffPolicyFactory = new LoadBalancedBackOffPolicyFactory.NoBackOffPolicyFactory();


    @Mock
    private ServiceInstance instance;

    @Before
    public void setUp() throws Exception {
        lbProperties = new LoadBalancerRetryProperties();
        lbRequestFactory = mock(LoadBalancerRequestFactory.class);

    }

    @Test
    public void getBaseUri() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$.[?(@.serviceName=='service1')].links[0].rel", is(Collections.singletonList("self"))))
                .andExpect(jsonPath("$.[?(@.serviceName=='service1')].links[0].href", is(Collections.singletonList("http://localhost/service1"))))
                .andExpect(jsonPath("$.[?(@.serviceName=='service2')].links[0].rel", is(Collections.singletonList("self"))))
                .andExpect(jsonPath("$.[?(@.serviceName=='service2')].links[0].href", is(Collections.singletonList("http://localhost/service2"))));
        ;
    }

    @Test
    public void getserviceUri() throws Exception {
        when(client.choose("s1-1")).thenReturn(instance);
        // mockMvc.perform(get("/s1-1"))
        //         .andExpect(status().isOk());
    }

    @Test
    public void getoperateUri() throws Exception {
        // HttpRequest request = mock(HttpRequest.class);
        // when(request.getURI()).thenReturn(new URI("http://foo"));
        // ClientHttpResponse clientHttpResponse = new MockClientHttpResponse(new byte[]{}, HttpStatus.OK);
        // LoadBalancedRetryPolicy policy = mock(LoadBalancedRetryPolicy.class);
        // InterceptorRetryPolicy interceptorRetryPolicy = new InterceptorRetryPolicy(request, policy, client, "foo");
        // LoadBalancedRetryPolicyFactory lbRetryPolicyFactory = mock(LoadBalancedRetryPolicyFactory.class);
        // when(lbRetryPolicyFactory.create(eq("foo"), any(ServiceInstanceChooser.class))).thenReturn(policy);
        // ServiceInstance serviceInstance = mock(ServiceInstance.class);
        // when(client.choose(eq("foo"))).thenReturn(serviceInstance);
        // when(client.execute(eq("foo"), eq(serviceInstance), any(LoadBalancerRequest.class))).thenReturn(clientHttpResponse);
        // when(this.lbRequestFactory.createRequest(any(), any(), any())).thenReturn(mock(LoadBalancerRequest.class));
        // lbProperties.setEnabled(true);
        // RetryLoadBalancerInterceptor interceptor = new RetryLoadBalancerInterceptor(client, lbProperties, lbRetryPolicyFactory,
        //         lbRequestFactory, backOffPolicyFactory);
        // mockMvc.perform(get("/foo/operate"))
        //         .andExpect(status().isOk());
    }
}
